package client;

import java.io.FileNotFoundException;

import models.GroupGame;

public class GroupGamesClient {

	public static void main(String[] args) throws FileNotFoundException {
		//first pay off matrix
		int[][] firstPayOffMatrix = {{1,1},{5,5},{5,5},{1,1}};
		GroupGame firstGame = new GroupGame(firstPayOffMatrix, "groupGameData/firstGameRatio.csv", "groupGameData/firstGameQValues.csv");
		firstGame.playGame();

		//second pay off matrix
		int[][] secondPayOffMatrix = {{1,1},{5,5},{3,3},{1,1}};
		GroupGame secondGame = new GroupGame(secondPayOffMatrix, "groupGameData/secondGameRatio.csv", "groupGameData/secondGameQValues.csv");
		secondGame.playGame();

		//third pay off matrix
		int[][] thirdPayOffMatrix = {{1,1},{3,5},{5,3},{1,1}};
		GroupGame thirdGame = new GroupGame(thirdPayOffMatrix, "groupGameData/thirdGameRatio.csv", "groupGameData/thirdGameQValues.csv");
		thirdGame.playGame();

		//fourth pay off matrix
		int[][] fourthPayOffMatrix = {{3,3},{0,5},{5,0},{1,1}};
		GroupGame fourthGame = new GroupGame(fourthPayOffMatrix, "groupGameData/fourthGameRatio.csv", "groupGameData/fourthGameQValues.csv");
		fourthGame.playGame();

		//fifth pay off matrix
		int[][] fifthPayOffMatrix = {{2,2},{0,5},{5,0},{1,1}};
		GroupGame fifthGame = new GroupGame(fifthPayOffMatrix, "groupGameData/fifthGameRatio.csv", "groupGameData/fifthGameQValues.csv");
		fifthGame.playGame();
	}

}
