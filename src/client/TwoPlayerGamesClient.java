package client;

import java.io.FileNotFoundException;

import models.TwoPlayerGame;

public class TwoPlayerGamesClient {

    public static void main(String[] args) throws FileNotFoundException {
        //first pay off matrix
        int[][] firstPayOffMatrix = {{1,1},{5,5},{5,5},{1,1}};
        TwoPlayerGame firstGame = new TwoPlayerGame(firstPayOffMatrix, "data/firstGameData.csv");
        firstGame.playGame();

        //second pay off matrix
        int[][] secondPayOffMatrix = {{1,1},{5,5},{3,3},{1,1}};
        TwoPlayerGame secondGame = new TwoPlayerGame(secondPayOffMatrix, "data/secondGameData.csv");
        secondGame.playGame();

        //third pay off matrix
        int[][] thirdPayOffMatrix = {{1,1},{3,5},{5,3},{1,1}};
        TwoPlayerGame thirdGame = new TwoPlayerGame(thirdPayOffMatrix, "data/thirdGameData.csv");
        thirdGame.playGame();

        //fourth pay off matrix
        int[][] fourthPayOffMatrix = {{3,3},{0,5},{5,0},{1,1}};
        TwoPlayerGame fourthGame = new TwoPlayerGame(fourthPayOffMatrix, "data/fourthGameData.csv");
        fourthGame.playGame();
        
        //fifth pay off matrix
        int[][] fifthPayOffMatrix = {{2,2},{0,5},{5,0},{1,1}};
        TwoPlayerGame fifthGame = new TwoPlayerGame(fifthPayOffMatrix, "data/fifthGameData.csv");
        fifthGame.playGame();
    }
}