package models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

public class TwoPlayerGame {
    private static final int GAME_ITERATION = 1000;
    private Agent[] agents = new Agent[2];
    private int[][] payOffMatrix = new int[4][2];
    private String outputDataFile;

    public TwoPlayerGame(int[][] payOffMatrix, String outputDataFile) {
        this.payOffMatrix = payOffMatrix;
        this.outputDataFile = outputDataFile;
    }

    //Play the game GAME_ITERATION times
    public void playGame() throws FileNotFoundException {
        constructAgents();
        PrintStream outputStream = constructOutputStream();
        outputStream.println("firstAgent_Q(c),firstAgent_Q(d),secondAgent_Q(c),secondAgent_Q(d)");

        for(int i = 0; i < GAME_ITERATION; i++) {
            //get action and join-action
            char firstAction = agents[0].nextAction();
            char secondAction = agents[1].nextAction();
            String joinActionFirst = Character.toString(firstAction) + secondAction;
            String joinActionSecond = Character.toString(secondAction) + firstAction;

            //Update Q-values
            agents[0].updateQValues(firstAction, agents[0].getUtility(joinActionFirst));
            agents[1].updateQValues(secondAction, agents[1].getUtility(joinActionSecond));

            //Output Q-values after each play to file
            outputStream.println(agents[0].getQValues().get('c') + "," + agents[0].getQValues().get('d') + "," + agents[1].getQValues().get('c') + "," + agents[1].getQValues().get('d'));
        }

        outputStream.close();
    }

    //Construct HashMap pay off matrix from array
    @SuppressWarnings("unchecked")
    private Map<String, Integer>[] constructPayOffMatrix() {
        HashMap<String, Integer> first = new HashMap<String, Integer>();
        first.put("cc", payOffMatrix[0][0]);
        first.put("cd", payOffMatrix[1][0]);
        first.put("dc", payOffMatrix[2][0]);
        first.put("dd", payOffMatrix[3][0]);

        HashMap<String, Integer> second = new HashMap<String, Integer>();
        second.put("cc", payOffMatrix[0][1]);
        second.put("dc", payOffMatrix[1][1]);
        second.put("cd", payOffMatrix[2][1]);
        second.put("dd", payOffMatrix[3][1]);

       return new HashMap[] {first, second};
    }

    //Contruct agents for game
    private void constructAgents() {
        Map<String, Integer>[] payOffMatrixes = constructPayOffMatrix();
        this.agents[0] = new Agent(payOffMatrixes[0]);
        this.agents[1] = new Agent(payOffMatrixes[1]);
    }

    //Construct the output stream to write game data to files
    private PrintStream constructOutputStream() throws FileNotFoundException {
        return new PrintStream(new File(outputDataFile));
    }
}
