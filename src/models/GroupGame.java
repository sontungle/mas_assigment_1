package models;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class GroupGame {
	private static final int NUMBER_EACH_GROUP = 20;
	private static final int GAME_ITERATION = 20000;
	private Agent[] groupA = new Agent[NUMBER_EACH_GROUP];
	private Agent[] groupB = new Agent[NUMBER_EACH_GROUP];
	private int[][] payOffMatrix = new int[4][2];
	private String outputRatioFile;
	private String outputQValueFile;

	public GroupGame(int[][] payOffMatrix, String outputRatioFile, String outputQValueFile) {
		this.payOffMatrix = payOffMatrix;
		this.outputRatioFile = outputRatioFile;
		this.outputQValueFile = outputQValueFile;
	}
	
	//Play the game GAME_ITERATION times
	public void playGame() throws FileNotFoundException {
		//Create output stream to file
		PrintStream[] outputs = constructOutputStream();
		PrintStream outputRatioStream = outputs[0];
		PrintStream outputQValueStream = outputs[1];
		//File headers
		outputRatioStream.println("Q(c) better, Q(d) better, Q(c) better, Q(d) better");
		outputQValueStream.println("Group A,,Group B");

		constructAgents();
		for(int i = 0; i < GAME_ITERATION; i++) {
			Agent firstAgent = groupA[new Random().nextInt(20)];
			Agent secondAgent = groupB[new Random().nextInt(20)];

			//Print out strategies ratio
			outputRatioStream.println(getActionRatiosGroup(groupA)[0] + "," + getActionRatiosGroup(groupA)[1] + "," + getActionRatiosGroup(groupB)[0] + "," + getActionRatiosGroup(groupB)[1]);

			//get action and join-action
            char firstAction = firstAgent.nextAction();
            char secondAction = secondAgent.nextAction();
            String joinActionFirst = Character.toString(firstAction) + secondAction;
            String joinActionSecond = Character.toString(secondAction) + firstAction;

            //Update Q-values
            firstAgent.updateQValues(firstAction, firstAgent.getUtility(joinActionFirst));
            secondAgent.updateQValues(secondAction, secondAgent.getUtility(joinActionSecond));
		}
		
		//Out put final Q-values in each group to file
		for(int i = 0; i < NUMBER_EACH_GROUP; i++) {
			outputQValueStream.println(groupA[i].getQValues() + "," + groupB[i].getQValues());
		}
		
		//close out put streams
		outputRatioStream.close();
		outputQValueStream.close();
	}

	//Construct the payoff matrix from array to HashMap
	@SuppressWarnings("unchecked")
    private Map<String, Integer>[] constructPayOffMatrix() {
        HashMap<String, Integer> first = new HashMap<String, Integer>();
        first.put("cc", payOffMatrix[0][0]);
        first.put("cd", payOffMatrix[1][0]);
        first.put("dc", payOffMatrix[2][0]);
        first.put("dd", payOffMatrix[3][0]);

        HashMap<String, Integer> second = new HashMap<String, Integer>();
        second.put("cc", payOffMatrix[0][1]);
        second.put("dc", payOffMatrix[1][1]);
        second.put("cd", payOffMatrix[2][1]);
        second.put("dd", payOffMatrix[3][1]);

       return new HashMap[] {first, second};
    }
	
	//Construct the agents to play the game
	private void constructAgents() {
		Map<String, Integer>[] payOffMatrixes = constructPayOffMatrix();
		for(int i = 0; i < NUMBER_EACH_GROUP; i++) {
			groupA[i] = new Agent(payOffMatrixes[0]);
			groupB[i] = new Agent(payOffMatrixes[1]);
		}
	}

	//Construct the output stream to write game data to files
	private PrintStream[] constructOutputStream() throws FileNotFoundException {
		return new PrintStream[] {new PrintStream(new File(outputRatioFile)), new PrintStream(new File(outputQValueFile))};
	}

	//Get ratio of better strategy in a group
	private double[] getActionRatiosGroup(Agent[] group) {
		int actC = 0;
		int actD = 0;
		for(Agent agent : group) {
			if(agent.getQValues().get('c') > agent.getQValues().get('d'))
				actC++;
			else if(agent.getQValues().get('c') < agent.getQValues().get('d'))
				actD++;
		}
		double ratiosC = (double) actC / NUMBER_EACH_GROUP;
		double ratiosD = (double) actD / NUMBER_EACH_GROUP;

		return new double[] {ratiosC, ratiosD};
	}
}
