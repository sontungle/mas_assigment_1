package models;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Agent {
    private Map<String, Integer> payOffMatrix;
    private Map<Character, Double> QValues;
    private char[] possibleActions = { 'c', 'd' };
    private double epsilon = 0.05;
    private double alpha = 0.05;

    public Agent(Map<String, Integer> payOffMatrix) {
        QValues = new HashMap<Character, Double>();
        QValues.put('c', 1.0);
        QValues.put('d', 1.0);

        this.payOffMatrix = payOffMatrix;
    }

    public Map<Character, Double> getQValues() {
        return QValues;
    }

    //Return next action
    public char nextAction() {
        int index = indexOfBetterAction();
        if (index != -1) {
            char better = possibleActions[index];
            char worse = possibleActions[1 - index];
            return new Random().nextDouble() <= (1.0 - epsilon) ? better : worse;
        } else {
            return possibleActions[new Random().nextInt(2)];
        }
    }

    //Return index of action with better Q-value
    private int indexOfBetterAction() {
        if (QValues.get('c') > QValues.get('d')) {
            return 0;
        } else if (QValues.get('d') > QValues.get('c')) {
            return 1;
        } else {
            return -1;
        }
    }

    //Get utility based on join-actions and payoff matrix
    public int getUtility(String joinAction) {
        return payOffMatrix.get(joinAction);
    }

    //Update q-value based on action did and utility receive
    public void updateQValues(char action, int utility) {
        double value = QValues.get(action);
        value = alpha * utility + (1 - alpha) * value;
        QValues.put(action, value);
    }
}